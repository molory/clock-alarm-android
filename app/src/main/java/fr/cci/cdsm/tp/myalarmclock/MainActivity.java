package fr.cci.cdsm.tp.myalarmclock;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements TimePicker.OnTimeChangedListener, CompoundButton.OnCheckedChangeListener {

    public final static int REQUEST_ALARM_RECEIVER = 1234;

    private Switch mSwitch;

    private TimePicker mTimePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwitch = (Switch) findViewById(R.id.switchEnable);

        mTimePicker = (TimePicker) findViewById(R.id.timePicker);
        mTimePicker.setIs24HourView(true);

        Date dateFromPrefs = AlarmUtils.getAlarmDateFromPrefs(this);
        boolean isDateFromPrefsPresent = dateFromPrefs != null;
        if (isDateFromPrefsPresent) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFromPrefs);

            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            setPickerHour(hour);
            setPickerMinute(minute);
            mSwitch.setChecked(true);
        }

        mSwitch.setChecked(isDateFromPrefsPresent);

        mSwitch.setOnCheckedChangeListener(this);
        mTimePicker.setOnTimeChangedListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        updateAlarm();
        if (!isChecked) {
            AlarmUtils.deleteAlarmFromPrefs(this);
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        updateAlarm();
    }

    private void updateAlarm() {
        AlarmUtils.cancelAlarm(this);
        if (mSwitch.isChecked()) {
            AlarmUtils.createAlarm(this, getTimePicked());
        }
    }

    private Date getTimePicked() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, getPickerHour());
        calendar.set(Calendar.MINUTE, getPickerMinute());
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    private int getPickerHour() {
        if (Build.VERSION.SDK_INT >= 23)
            return mTimePicker.getHour();
        else
            return mTimePicker.getCurrentHour();
    }

    private void setPickerHour(int hour) {
        if (Build.VERSION.SDK_INT >= 23)
            mTimePicker.setHour(hour);
        else
            mTimePicker.setCurrentHour(hour);
    }

    private int getPickerMinute() {
        if (Build.VERSION.SDK_INT >= 23)
            return mTimePicker.getMinute();
        else
            return mTimePicker.getCurrentMinute();
    }

    private void setPickerMinute(int minute) {
        if (Build.VERSION.SDK_INT >= 23)
            mTimePicker.setMinute(minute);
        else
            mTimePicker.setCurrentMinute(minute);
    }
}
