package fr.cci.cdsm.tp.myalarmclock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Date;

public class AlarmUtils {

    private final static String PREF_ALARM = "ALARM";

    public static void createAlarm(Context context, Date date) {
        createAlarm(context, date, true);
    }

    public static void restoreAlarmIfNecessary(Context context) {
        Date date = getAlarmDateFromPrefs(context);
        if (date != null) {
            createAlarm(context, date, false); // no need to save in prefs
        }
    }

    public static Date getAlarmDateFromPrefs(Context context) {
        long timeInMillions = getPrefs(context).getLong(PREF_ALARM, 0);
        if (timeInMillions > 0) {
            return new Date(timeInMillions);
        } else {
            return null;
        }
    }

    public static void cancelAlarm(Context context) {
        PendingIntent pendingIntent = getPendingIntent(context);
        if (pendingIntent != null) {
            getAlarmManager(context).cancel(pendingIntent);
        }
    }

    public static void createAlarm(Context context, Date date, boolean saveAlarmInPrefs) {
        long timeInMillis = date.getTime();
        boolean isInPast = timeInMillis <= System.currentTimeMillis();

        if (isInPast) {
            timeInMillis += 1000 * 60 * 60 * 24;
        }

        getAlarmManager(context).set(AlarmManager.RTC_WAKEUP, timeInMillis, getPendingIntent(context));
        if (saveAlarmInPrefs) {
            saveAlarmInPrefs(context, timeInMillis);
        }
    }

    public static void deleteAlarmFromPrefs(Context context) {
        saveAlarmInPrefs(context, 0);
    }

    private static void saveAlarmInPrefs(Context context, long timeInMillis) {
        SharedPreferences sharedPref = getPrefs(context);
        if (timeInMillis == 0) {
            sharedPref.edit().remove(PREF_ALARM).apply();
        } else {
            sharedPref.edit().putLong(PREF_ALARM, timeInMillis).apply();
        }
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static PendingIntent getPendingIntent(Context context) {
        Intent myIntent = new Intent(context, MyAlarmBroadcastReceiver.class);
        return PendingIntent.getBroadcast(context, MainActivity.REQUEST_ALARM_RECEIVER, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static AlarmManager getAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

}
